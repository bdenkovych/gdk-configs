# See https://github.com/guard/listen?tab=readme-ov-file#increasing-the-amount-of-inotify-watchers

set -e

sudo sed -i "1ifs.inotify.max_user_watches=524288\n" /etc/sysctl.conf
sudo sysctl -p
