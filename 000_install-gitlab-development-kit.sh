# See https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md

set -e

if [[ -d ~/gdk/ ]]; then
  echo "~/gdk/ exists."
  exit 1
fi

cd ~

git clone https://gitlab.com/gitlab-org/gitlab-development-kit.git gdk

cd ~/gdk/

cat <<EOF | tee gdk.yml > /dev/null
asdf:
  opt_out: true
mise:
  enabled: true
EOF

make bootstrap

gdk install blobless_clone=false gitlab_repo=git@gitlab.com:gitlab-org/gitlab.git

gdk stop

rm -f ~/gdk/gdk.yml
ln -s ~/work/gitlab/gitlab.com/bdenkovych/gdk-configs/gdk.yml ~/gdk/

# See https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/nginx.md
mkcert -install
mkcert gdk.test

gdk reconfigure

gdk stop

source ~/work/gitlab/gitlab.com/bdenkovych/gdk-configs/003_lefthook_setup.sh
source ~/work/gitlab/gitlab.com/bdenkovych/gdk-configs/007_env-runit_setup.sh

# Manual steps

# Configure gitlab/config/gitlab.yml based on gdk.yml

# Activate GitLab EE with a license file or key: https://docs.gitlab.com/ee/user/admin_area/license_file.html
# See ~/work/gitlab/files/licenses/

# Empty "Documentation pages URL" to not redirect /help pages
# See https://docs.gitlab.com/ee/administration/settings/help_page.html#redirect-help-pages

# Set up SAML SSO for https://gdk.test:3443/zebraokta
# https://developer.okta.com
# Identity provider single sign-on URL: https://dev-86260235.okta.com/app/dev-86260235_gdkzebraoktasamlsso_1/exkkwyve1ezJz85vS5d7/sso/saml
# Certificate fingerprint: 31:83:E0:6B:52:00:74:C7:A5:E0:A2:A2:02:A2:31:B2:5A:46:6B:1B
