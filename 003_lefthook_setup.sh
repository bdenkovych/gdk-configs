# See https://docs.gitlab.com/ee/development/contributing/style_guides.html#skip-or-enable-a-specific-lefthook-check

set -e

rm -f ~/gdk/gitlab/lefthook-local.yml
ln -s ~/work/gitlab/gitlab.com/bdenkovych/gdk-configs/lefthook-local.yml ~/gdk/gitlab/
