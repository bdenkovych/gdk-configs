# See https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kerberos.md

set -e

sudo sed -i "1i127.0.0.1 krb5.gdk.test\n" /etc/hosts
