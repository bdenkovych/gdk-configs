# See https://docs.gitlab.com/ee/development/database/database_lab.html#access-the-console-with-psql

set -e

cat <<EOF | tee ~/.ssh/config > /dev/null
Host lb-bastion.db-lab.gitlab.com
  User bdenkovych
  IdentitiesOnly yes
  IdentityFile ~/.ssh/id_ed25519

Host *.gitlab-db-lab.internal
  User bdenkovych
  PreferredAuthentications publickey
  IdentitiesOnly yes
  IdentityFile ~/.ssh/id_ed25519
  ProxyCommand ssh lb-bastion.db-lab.gitlab.com -W %h:%p
EOF

chmod 600 ~/.ssh/config
