# See https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md

set -e

sudo sed -i "1i127.0.0.1 gdk.test\n" /etc/hosts
