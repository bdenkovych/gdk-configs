# See https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/runit.md

set -e

rm -f ~/gdk/env.runit
ln -s ~/work/gitlab/gitlab.com/bdenkovych/gdk-configs/env.runit ~/gdk/
