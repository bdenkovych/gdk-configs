# See https://handbook.gitlab.com/handbook/security/corporate/systems/sentinelone/setup/

set -e

if [[ ! -d ~/work/gitlab/gitlab.com/gitlab-com/it/security/sentinelone-installers/ ]]; then
  echo "~/work/gitlab/gitlab.com/gitlab-com/it/security/sentinelone-installers/ does not exist."
  exit 1
fi

echo "Have you configured the config.cfg file and ready to install the package?"
echo -n "Type 'yes' or the operation will be cancelled: "
read ready_to_install_the_package_answer
if [[ "$ready_to_install_the_package_answer" != "yes" ]]; then
  echo ""
  echo "Cancelled!"
  echo ""
  exit 1
fi

sudo S1_AGENT_INSTALL_CONFIG_PATH=~/work/gitlab/gitlab.com/gitlab-com/it/security/sentinelone-installers/config.cfg apt install ~/work/gitlab/gitlab.com/gitlab-com/it/security/sentinelone-installers/SentinelAgent_linux_x86_64_v24_1_2_6.deb
