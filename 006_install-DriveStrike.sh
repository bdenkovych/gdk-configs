# See https://handbook.gitlab.com/handbook/it/end-user-services/onboarding-access-requests/#fleet-intelligence--remote-lockwipe

# See https://app.drivestrike.com/instructions/linux/

set -e

curl -L https://app.drivestrike.com/static/drivestrike/dsinstall-apt.sh -o /tmp/dsinstall-apt.sh

chmod +x /tmp/dsinstall-apt.sh

sudo /tmp/dsinstall-apt.sh

rm /tmp/dsinstall-apt.sh
