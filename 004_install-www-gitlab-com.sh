# See https://gitlab.com/gitlab-com/www-gitlab-com

set -e

if [[ -d ~/www-gitlab-com/ ]]; then
  echo "~/www-gitlab-com/ exists."
  exit 1
fi

cd ~

git clone git@gitlab.com:gitlab-com/www-gitlab-com.git

# See https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/doc/development.md
